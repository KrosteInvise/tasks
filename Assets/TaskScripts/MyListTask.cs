﻿using System;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using UnityEngine;

public class MyListTask : MonoBehaviour
{
    [SerializeField] int item;

    [SerializeField] int index;

    MyList<int> testList = new MyList<int>();

    [Button()]
    void Count()
    {
        var count = testList.Count;
        Debug.Log($"Длина листа: '{count}'");
    }

    [Button()]
    void Capacity()
    {
        var capacity = testList.Capacity;
        Debug.Log($"Вместимость листа: '{capacity}'");
    }

    [Button()]
    void Add()
    {
        testList.Add(item);
        Debug.Log($"Добавлен элемент: '{item}'");
    }

    [Button()]
    void Insert()
    {
        testList.Insert(item, index);
        Debug.Log($"Добавлен элемент '{item}' на '{index}' позицию");
    }

    [Button()]
    void RemoveAt()
    {
        testList.RemoveAt(index);
        Debug.Log($"Удалён элемент с '{index}' позиции");
    }

    [Button()]
    void Clear()
    {
        testList.Clear();
        Debug.Log("Лист очищен");
    }
}

public class MyList<T>
{
    int count;
    T[] myList;

    public MyList()
    {
        myList = new T[0];
    }

    public int Count => count;

    public int Capacity
    {
        get => myList.Length;
        set
        {
            if (value < count)
                throw new ArgumentOutOfRangeException();
            if (value == myList.Length)
                return;
            if (value > 0)
            {
                T[] newArray = new T[value];
                if (count > 0)
                    Array.Copy(myList, 0, newArray, 0, count);
                myList = newArray;
            }
        }
    }

    public T this[int i]
    {
        get 
        {
            if(i > count)
                throw new ArgumentOutOfRangeException();
            return myList[i]; 
        }
        set 
        { 
            if(i > count)
                throw new ArgumentOutOfRangeException();
            myList[i] = value;
        }
    }

    void CheckArraySpace()
    {
        if (count == Capacity)
        {
            int num = myList.Length == 0 ? 4 : myList.Length * 2;
            Capacity = num;
        }
    }

    public void Add(T a)
    {
        CheckArraySpace();
        
        this[count] = a;
        count++;
    }

    public void Insert(T a, int index)
    {
        
        if (index > Capacity)
            throw new IndexOutOfRangeException();
        
        CheckArraySpace();

        this[index] = a;
        count++;
    }

    public void RemoveAt(int index)
    {
        if (index > Capacity)
            throw new IndexOutOfRangeException();

        CheckArraySpace();
        
        this[index] = default;
        count--;
    }

    public void Clear()
    {
        if (count > 0)
        {
            for (int i = 0; i < Count; i++)
                myList[i] = default;
            
            count = 0;
        }
    }
    
    public void TrimExcess()
    {
        if (myList.Length != Count)
            myList = InheritNewArray(myList, Count);
    }
    
    T[] InheritNewArray(T[] inheritArray, int length)
    {
        T[] newArray = new T[length];
		
        Array.Copy(inheritArray, newArray, length);

        return newArray;
    }
    
    public bool Contains(T item)
    {
        for (int i = 0; i < Count; i++)
        {
            if (myList[i].Equals(item))
                return true;
        }

        return false;
    }
}