﻿using NaughtyAttributes;
using UnityEngine;

public class PhoneListView : MonoBehaviour
{
    [Header("Input fields")]
    [SerializeField]
    long number;

    [SerializeField]
    string name;

    [SerializeField]
    string adress;

    PhoneListBase pb = new PhoneListBase();

    [Button("Add user")]
    void AddUser()
    {
        var user = new User(name, adress);

        pb.AddUser(number, user);
        Debug.Log($"User number: '{number}', Name: '{name}', Lives here: '{adress}' was added");
    }

    [Button("Delete user")]
    void RemoveUser()
    {
        if (pb.RemoveUser(number))
            Debug.Log($"User with number {number} was deleted");
        else
            Debug.Log($"Number with '{number}' not found");
    }

    [Button("Find by name")]
    void FindNumbersByName()
    {
        foreach (var pos in pb.SearchNumbersByName(name))
            Debug.Log($"User found with number: {pos.Key}");

        if (pb.SearchNumbersByName(name).Count == 0)
            Debug.Log("Nothing found");
    }

    [Button("Replace name")]
    void ReplaceName()
    {
        var user = new User(name, adress);
        var foundedUser = pb.SearchForNumber(number);

        foundedUser.name = name;
        pb.ReplaceName(number, name);
        Debug.Log($"User old name was replaced on new name: '{user.name}'");
    }

    public class User
    {
        public string name;
        public string adress;

        public User(string name, string adress)
        {
            this.name = name;
            this.adress = adress;
        }
    }
}
