﻿using System.Collections.Generic;
using UnityEngine.Assertions;
using static PhoneListView;

public class PhoneListBase
{
    Dictionary<long, User> phoneList = new Dictionary<long, User>();
    
    public void AddUser(long number, User user)
    {
        Assert.IsFalse(string.IsNullOrWhiteSpace(user.name));

        phoneList.Add(number, user);
    }

    public bool RemoveUser(long number)
    {
        return phoneList.Remove(number);
    }

    public Dictionary<long, User> SearchNumbersByName(string name)
    {
        var sortedDictionary = new Dictionary<long, User>();

        foreach (var pos in phoneList)
        {
            if (pos.Value.name.Contains(name))
                sortedDictionary.Add(pos.Key, pos.Value);
        }

        return sortedDictionary;
    }

    public User SearchForNumber(long number)
    {
        if (phoneList.TryGetValue(number, out User user))
            return user;
        
        return null;
    }

    public void ReplaceName(long number, string name)
    {
        phoneList[number].name = name;
    }
}
