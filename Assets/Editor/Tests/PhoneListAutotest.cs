﻿using NUnit.Framework;
using static PhoneListView;
using Assert = UnityEngine.Assertions.Assert;

public class PhoneListAutotest
{
    long number = 79232275258;
    long number2 = 79232281488;
    long number3 = 79602281488;
    
    [Test]
    public void AddRemoveTest()
    {
        PhoneListBase pb = new PhoneListBase();
        User user = new User("Вася", "Ленина 3");
        User user2 = new User("Петя", "Павлова 7");
        
        pb.AddUser(number, user);
        User findedUser = pb.SearchForNumber(number);
        Assert.IsTrue(findedUser == user);
        
        pb.AddUser(number2, user2);
        User findedUser2 = pb.SearchForNumber(number2);
        Assert.IsTrue(findedUser2 == user2);

        pb.RemoveUser(number2);
        User deletedUser2 = pb.SearchForNumber(number2);
        Assert.IsFalse(deletedUser2 == user);
    }

    [Test]
    public void ReplaceNameTest()
    {
        PhoneListBase pb   = new PhoneListBase();
        User user = new User("Вася", "Ленина 3");
        string replacedName = "Степа";
        
        pb.AddUser(number, user);
        User findedUser = pb.SearchForNumber(number);
        Assert.IsTrue(findedUser == user);
        
        pb.ReplaceName(number, replacedName);
        Assert.IsTrue(replacedName == user.name);
    }

    [Test]
    public void SearchNumbersByNameTest()
    {
        PhoneListBase pb = new PhoneListBase();
        User user = new User("Вася", "Ленина 3");
        User user2 = new User("Вася", "Павлова 7");
        User user3 = new User("Петя", "Лазо 4");
        string searchingName = "Вася";

        pb.AddUser(number, user);
        pb.AddUser(number2, user2);
        pb.AddUser(number3, user3);

        var sortedDb = pb.SearchNumbersByName(searchingName);

        foreach (var pos in sortedDb)
            Assert.IsTrue(pos.Value.name == searchingName);
    }
}
